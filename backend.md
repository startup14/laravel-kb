#### DB
* [Choosing the best indexes for MySQL query optimization](https://www.eversql.com/choosing-the-best-indexes-for-mysql-query-optimization/)
* []()
* []()
* []()
* []()

#### Performance
* [Running PHP code in parallel, the easy way](https://stitcher.io/blog/parallel-php)
* [Extreme HTTP Performance Tuning: 1.2M API req/s on a 4 vCPU EC2 Instance](https://talawah.io/blog/extreme-http-performance-tuning-one-point-two-million/)
* []()
* []()
* []()
* []()

#### Architecture
* [Keep controllers clean by using form requests in Laravel](https://freek.dev/1952-keep-controllers-clean-by-using-form-requests-in-laravel)
* []()
* []()
* []()
* []()

#### Frontend
* [A free repository for community components using TailwindCSS](https://tailwindcomponents.com/)
* [Design Resources for WEB developers](https://github.com/bradtraversy/design-resources-for-developers)
* [Using Laravel translations in Javascript with the Laravel Translations Loader](https://laravel-news.com/laravel-translations-in-javascript)
* [My current HTML boilerplate](https://www.matuzo.at/blog/html-boilerplate/)
* [Refactoring to Inertia Forms](https://engineering.aryeo.com/refactoring-to-inertia-forms)
* [Vite with Laravel](https://sebastiandedeyne.com/vite-with-laravel/#:~:text=Vite%20is%20a%20frontend%20build,Rollup%20to%20bundle%20the%20assets)
